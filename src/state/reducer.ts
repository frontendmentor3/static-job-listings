import {State} from './state'

export type Action =
    | {
    type: 'SET_FILTER';
    payload: string[]
}
    | {
    type: 'REMOVE_SINGLE_FILTER_ITEM';
    payload: string
}
    | {
    type: 'ADD_SINGLE_FILTER_ITEM';
    payload: string
}|{
    type: 'REMOVE_ALL_FILTERS';
    payload: null
};

export const reducer = (state: State, action: Action): State => {
    switch (action.type) {
        case 'SET_FILTER':
            return {
                ...state,
                activeFilters: action.payload
            }
        case 'REMOVE_SINGLE_FILTER_ITEM':
            return {
                ...state,
                activeFilters: state.activeFilters.filter(f => f !== action.payload)
            }
        case 'ADD_SINGLE_FILTER_ITEM':
            if(state.activeFilters.includes(action.payload)){
                return state;
            }else{
                return {
                    ...state,
                    activeFilters: [...state.activeFilters, action.payload]
                }
            }
        case 'REMOVE_ALL_FILTERS':
            return {
                ...state,
                activeFilters: []
            }

        default:
            return state;

    }
}

export const removeSingleFilter = (filter: string): Action => {
    return {
        type: 'REMOVE_SINGLE_FILTER_ITEM',
        payload: filter
    }
}


export const addSingleFilter = (filter: string):Action => {
    return {
        type: 'ADD_SINGLE_FILTER_ITEM',
        payload: filter
    }
}

export const removeAllFilters = ():Action => {
    return {
        type:'REMOVE_ALL_FILTERS',
        payload: null
    }
}

