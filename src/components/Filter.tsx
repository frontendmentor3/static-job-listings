import styled from 'styled-components'
import {removeAllFilters, removeSingleFilter, useStateValue} from "../state";

type FilterProps = {
    filter: string[]
}

type FilterItemProps = {
    filterItem: string
}

const FilterContainerStyles = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 20px;
  margin-bottom: 30px;
  background-color: var(--white);
  border-radius: 10px;
  box-shadow: 6px 6px 20px var(--light-grayish-cyan-shadow);
  
  
  .filter-items{
    display: flex;
    flex-wrap: wrap;
    gap: 10px;
  }
  
  .clear-button{
    color: var(--dark-grayish-cyan);
    font-weight: bold;
    cursor: pointer;
  }
`

const SingleFilterItemStyles = styled.div`
  display: flex;
  color: var(--desaturated-dark-cyan);
  background-color: var(--light-grayish-cyan-filter);    
  font-weight: bold;
  border-radius: 5px;
  
  .filter-text{
    padding: 10px;    
  }
  
  .remove-filter-item{
    color: var(--white);
    background-color: var(--desaturated-dark-cyan);
    padding: 10px;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    cursor: pointer;
  }
`

const SingleFilterItem = ({filterItem}:FilterItemProps) => {
    const [, dispatch] = useStateValue();
    return <SingleFilterItemStyles>
        <div className="filter-text">{filterItem}</div>
        <div className="remove-filter-item" onClick={()=>dispatch(removeSingleFilter(filterItem))}>X</div>
    </SingleFilterItemStyles>
}



const Filter = ({filter}:FilterProps) => {
    const [, dispatch] = useStateValue();
    return <FilterContainerStyles>
        <div className="filter-items">{filter.map(item=><SingleFilterItem filterItem={item} />)}</div>
        <div className="clear-button" onClick={()=>dispatch(removeAllFilters())}>Clear</div>
    </FilterContainerStyles>
}

export default Filter