import styled from "styled-components";
import {JobListingData} from "../types/Data";
import {extractFileName} from "../utils/extractFileName";
import {addSingleFilter, useStateValue} from "../state";
import {devices} from "../styles/Sizes";


type JobListingProps = {
    job: JobListingData
}

type JobListingContainterStyleProps = {
    featured: boolean
}

const JobListingContainerStyles = styled.div<JobListingContainterStyleProps>`
  margin: 30px 0px;
  background-color: var(--white);
  padding: 30px;
  border-radius: 10px;
  min-width: 250px;
  border-left: ${props => props.featured ? "10px solid var(--desaturated-dark-cyan)" : "none"};
  box-shadow: 6px 6px 20px var(--light-grayish-cyan-shadow);

  @media ${devices.desktop} {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`

const DesktopJobDetailStyles = styled.div`
  @media ${devices.desktop} {
    display: flex;
    align-items: center;
    gap: 30px;
  }
  
`

const CompanyLogoStyles = styled.img`
  position: relative;
  width: 55px;
  top: -60px;

  @media ${devices.desktop} {
    
    width: 90px;
    height: 90px;
    top: 0px;
  }
`

const JobHeaderStyles = styled.div`
  display: flex;
  gap: 10px;
  align-items: center;
  margin-top: -40px;

  .company-name {
    color: var(--desaturated-dark-cyan);
    font-weight: bold;
  }

  .new-tag {
    color: var(--white);
    background-color: var(--desaturated-dark-cyan);
    padding: 5px 10px 0px 10px;
    border-radius: 999px;
    text-transform: uppercase;
    font-size: 0.7rem;
    line-height: 1.2rem;
  }

  .featured-tag {
    color: var(--white);
    background-color: var(--black);
    padding: 5px 10px 0px 10px;
    border-radius: 999px;
    text-transform: uppercase;
    font-size: 0.7rem;
    line-height: 1.2rem;
  }

  @media ${devices.desktop} {
    margin-top: 0px;
  }

`

const JobTitleStyles = styled.div`
  margin-top: 20px;
  font-weight: bold;

`

const JobDetailsStyles = styled.div`
  display: flex;
  gap: 10px;
  margin: 20px 0px;
  font-size: 0.8rem;
  color: var(--dark-grayish-cyan);

`
const HrStyles = styled.hr`
  border: 0.5px solid var(--dark-grayish-cyan);
  @media ${devices.desktop} {
    display: none;
  }
`

const FilterTabletsContainerStyles = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 20px;
  margin: 20px 0px 0px 0px;
  cursor: pointer;

  .tags {
    background-color: var(--light-grayish-cyan-filter);
    color: var(--desaturated-dark-cyan);
    font-weight: bold;
    font-size: 0.8rem;
    padding: 10px;
    border-radius: 5px;
  }
`

const JobListing = ({job}: JobListingProps) => {
    const [, dispatch] = useStateValue();
    return <>
    <JobListingContainerStyles featured={job.featured}>
        <DesktopJobDetailStyles>
            <CompanyLogoStyles src={`/assets/images/logos/${extractFileName(job.logo)}.svg`}/>
            <div>
                <JobHeaderStyles>
                    <span className="company-name">{job.company}</span>
                    {job.new ? <span className="new-tag">new!</span> : ''}
                    {job.featured ? <span className="featured-tag">featured</span> : ''}
                </JobHeaderStyles>
                <JobTitleStyles className="job-title">{job.position}</JobTitleStyles>
                <JobDetailsStyles>
                    <div>{job.postedAt}</div>
                    &#9679;
                    <div>{job.contract}</div>
                    &#9679;
                    <div>{job.location}</div>
                </JobDetailsStyles>
                <HrStyles/>
            </div>
        </DesktopJobDetailStyles>

        <FilterTabletsContainerStyles>
            <span className="tags" onClick={() => dispatch(addSingleFilter(job.role))}>{job.role}</span>
            <span className="tags" onClick={() => dispatch(addSingleFilter(job.level))}>{job.level}</span>
            {job.languages.map(l => (
                <span className="tags" onClick={() => dispatch(addSingleFilter(l))}>{l}</span>
            ))}
            {job.tools.map(t => (
                <span className="tags" onClick={() => dispatch(addSingleFilter(t))}>{t}</span>
            ))}
        </FilterTabletsContainerStyles>




    </JobListingContainerStyles>
</>

}

export default JobListing