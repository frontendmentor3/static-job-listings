import data from '../data/data.json'
import {JobListingData} from "../types/Data";



const getAllRoles = (): string[] =>{
    return [...new Set(data.map(d=>d.role))]
}

const getAllLevels = (): string[] =>{
    return [...new Set(data.map(d=>d.level))]
}

const getAllLanguages = (): string[] => {
    return [...new Set(data.map(d=>d.languages).flat())]
}

const getAllTools = (): string[] => {
    return [...new Set(data.map(d=>d.tools).flat())]
}

const getAllJobListings = (): JobListingData[] => {
    return data
}

const jobListingsFunctions = {
    getAllRoles,
    getAllLanguages,
    getAllLevels,
    getAllTools,
    getAllJobListings
}

export default jobListingsFunctions