import {createGlobalStyle} from 'styled-components';
import {fontSize} from "./Sizes";


export const GlobalStyles = createGlobalStyle`
  :root{
    --desaturated-dark-cyan: hsl(180, 29%, 50%);
    
    
    --light-grayish-cyan-bg: hsl(180, 52%, 96%);
    --light-grayish-cyan-shadow: hsl(180, 22%, 80%);
    --light-grayish-cyan-filter:  hsl(180, 31%, 95%);
    --dark-grayish-cyan: hsl(180, 8%, 52%);
    --very-dark-grayish-cyan: hsl(180, 14%, 20%);
    
    --white: hsl(180,0%,100%);
    --black: hsl(0,0%,0%);

    font-family: 'Spartan', sans-serif;
    font-size: ${fontSize.primary};
  }
  
  body{
    margin: 0;
    padding: 0;
    background-color: var(--light-grayish-cyan-bg);
  }
`
