import jobListingsService from "./services/jobListings";
import JobListing from "./components/JobListing";
import styled from "styled-components";
import {useState} from "react";
import Filter from "./components/Filter";
import {useStateValue} from "./state";
import {JobListingData} from "./types/Data";
import {devices} from "./styles/Sizes";


const AppContainerStyles = styled.div`
  display: flex;
  flex-direction: column;
  padding: 20px;
  @media ${devices.desktop} {
    padding: 50px 70px;
    max-width: 1000px;
    margin: auto;
  }
`

function App() {
    const [jobs, ] = useState(jobListingsService.getAllJobListings())
    const [{activeFilters}, ] = useStateValue()

    const filterJobs = (jobs: JobListingData[]):JobListingData[] => {
        if (activeFilters.length===0) return jobs
        return jobs.filter(job=>{
            const fieldsToCheck = [job.role,
                ...job.languages,
                ...job.tools,
                job.level]

           return activeFilters.every(activeFilter=> fieldsToCheck.includes(activeFilter))
        })
    }

    return (
        <AppContainerStyles>
            {activeFilters.length!==0?
                <Filter filter={activeFilters}/>:""
            }
            {console.log(filterJobs(jobs))}
            {filterJobs(jobs)?.map(job => (
                <JobListing key={job.id} job={job}/>
            ))}
        </AppContainerStyles>
    );
}

export default App;

