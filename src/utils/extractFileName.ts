// returns the filename given a logo path in the JobListings data
// "./images/name.svg" -> name
export const extractFileName = (path:string):string => {
    return path.substring(9, path.length-4)
}

