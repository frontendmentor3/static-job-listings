import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {GlobalStyles} from "./styles/GlobalStyles";
import {reducer,StateProvider} from "./state";


ReactDOM.render(
  <StateProvider reducer={reducer}>
      <><GlobalStyles/>
          <App/>
      </>
  </StateProvider>,
  document.getElementById('root')
);


