# Frontend Mentor - Job listings with filtering solution

This is a solution to the [Job listings with filtering challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/job-listings-with-filtering-ivstIPCt). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)




## Overview

### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Filter job listings based on the categories

### Screenshot

![](screenshots/mobile_with_filter.png)
![](screenshots/desktop_with_filter.png)
![](screenshots/mobile_no_filter.png)



### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/static-job-listings)
- Live Site URL: [Netlify](https://inspiring-shockley-dea10c.netlify.app/)

## My process
1. import and write several functions to extract roles, etc from the data
2. mobile design
3. add state management (useContext, useReducer hooks)
4. desktop design

### Built with
- Flexbox
- CSS Grid
- Mobile-first workflow
- [React](https://reactjs.org/) - JS library
- [Typescript](https://www.typescriptlang.org/) - JavaScript with syntax for types
- [Styled Components](https://styled-components.com/) - For styles



### What I learned

```
error: Type 'Set' is not an array type
```
when using spread operator to convert set into array. The solution is to add `"downlevelIteration": true` to tsconfig.json
[Downlevel Iteration for ES3/ES5 in TypeScript](https://mariusschulz.com/blog/downlevel-iteration-for-es3-es5-in-typescript)

#### Styled Component Props with Typescript

```typescript
type StyleProps = {
featured: boolean
}

const JobListingContainerStyles = styled.div<StyleProps>`
    border-left: ${props => props.featured? "10px solid var(--desaturated-dark-cyan)":"none"};
`
```


### Continued development

Use this section to outline areas that you want to continue focusing on in future projects. These could be concepts you're still not completely comfortable with or techniques you found useful that you want to refine and perfect.

**Note: Delete this note and the content within this section and replace with your own plans for continued development.**

### Useful resources

- [React Architecture: How to Structure and Organize a React Application](https://www.taniarascia.com/react-architecture-directory-structure/) 
- [graphemica](https://graphemica.com/) - HTML entities
- [https://www.toptal.com/designers/htmlarrows/](https://www.toptal.com/designers/htmlarrows/) - HTML entities
- [Khanacademy - Unit: Color science](https://www.khanacademy.org/computing/pixar/color) - colours
- [Typescript State Handling with Context](https://fullstackopen.com/en/part9/react_with_types)
- [Context - React Docs](https://reactjs.org/docs/context.html#contextprovider)


## Author

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)


